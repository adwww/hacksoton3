$(function() {

	$('#scroller').isotope({});

	var socket = io.connect('http://hacksoton.local:3001/');

	d3.xml("/england.svg", function(xml) {
  		document.getElementById('map').appendChild(xml.documentElement);
  		$('#map svg').css({width: '100%'});
	});

	var regions = {
		'northern-ireland': 0,
		'scotland': 0,
		'wales': 0,
		'south-west': 0,
		'west-midlands': 0,
		'north-west': 0,
		'north-east': 0,
		'yorkshire-and-humber': 0,
		'east-midlands': 0,
		'east-england': 0,
		'south-east': 0,
		'london': 0
	};
	
  	socket.on('intro', function (data) {
    	console.log(data);
    	
    	var $newItem = $('<div class="item"></div>')
    		.addClass(data.service)
    		.append('<p>'+data.message+'</p>')
    		.end()
    		.append('<time>'+data.date+'</time>');

		$('#scroller')
			.prepend($newItem)
			.isotope('reloadItems')
			.isotope({ sortBy: 'original-order' });


		if (typeof data.region != 'undefined') {
			var region = data.region.toLowerCase().replace(/\s+/g, '-');
			updateRegionCount(regions, region);
		}

  	});

  	updateRegionCount(regions, 'london');
}); 

function updateRegionCount(regions, thisRegion) {

	var score = [
		'#0043C8',
		'#0049DC',
		'#014FF1',
		'#0659FF',
		'#3075FF',
		'#6497FF',
		'#82AAFF',
		'#BCD3FF',
		'#ffffff'
	];

	regions[thisRegion]++;

	var sortable = [];
	for (var region in regions) {
      sortable.push([region, regions[region]]);
	}

	sortable.sort(function(a, b) {return a[1] - b[1]});

	console.log('sortable', sortable);

	regions = {};
	$.each(sortable, function(i, el) {
		regions[el[0]] = el[1];
	});

	console.log(regions);



}

/**
 * Module dependencies.
 */

var express = require('express');
var DataSift = require('DataSift');
var mongoose = require('mongoose');
var io = require('socket.io').listen(3001);
var inside = require('point-in-polygon');
var locations = require('./geo/regions.json');

locations.features.forEach(function(region){

		region.geometry.coordinates.forEach(function(cord)
		{
		//	cord.reverse();

			if(region.geometry.type == 'Polygon') {

					cord.forEach(function(point)
					{
						point.reverse();
					});
			} else {
				cord[0].forEach(function(point){
						point.reverse();
				});
			}

		});

		console.log(region.geometry.type);
});



/*
* Mongoose Connect and Define Model
*/


mongoose.connect('mongodb://212.71.253.147/hacksoton');


var Intro = mongoose.model('Introduction', { 
	service: String,
	username: String,
	message: String,
	url: String,
	media_url: String,
	media_type: String,
	lng: Number,
	lat: Number,
	region: String,
	loc_text: String,
	date: { 
		type: Date, 
		default: Date.now 
	},
});

var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


//Initilalise the feed and connect
var consumer = new DataSift('phil_bennett', '497060c8d256f752030162086d6cb518');

consumer.connect();
//Connect to the stream

consumer.on("connect", function(){
	consumer.subscribe('0f38807478522b6ada7a447fad26bb47');
	console.log('Connected');
});

consumer.on("interaction", function(data){
	//console.log("Received data: " + JSON.stringify(data));

	//console.log(data);
	
	var  InteractionInstance  = new Intro({ 
		service: data.data.interaction.type,
		username: data.data.interaction.author.name,
		message: data.data.interaction.content,
		url: data.data.interaction.link,
		media_type: 'avatar',
		media_url: data.data.interaction.author.avatar,
		date: data.data.interaction.created_at,
	});


	if (data.data.interaction.geo !== undefined) {
	


		locations.features.forEach(function(region){
				
				if (region.type == "Polygon") {

					if (inside([data.data.interaction.geo.latitude, data.data.interaction.geo.longitude ],region.geometry.coordinates)){
						console.log("Main"+region.properties.GOR10NM);
					}
				} else
				{
					region.geometry.coordinates.forEach(function(points){

						if(inside([data.data.interaction.geo.latitude, data.data.interaction.geo.longitude ], points)) {
							console.log('Sub'+region.properties.GOR10NM);
							console.log(data.data.interaction.geo);
						}
					})
				}
		});


		InteractionInstance.set('lng',data.data.interaction.geo.longitude);
		InteractionInstance.set('lat',data.data.interaction.geo.latitude);
		
	
	}


	if(data.data.twitter.user !== undefined) {
			if(data.data.twitter.user.location !== undefined) {

				InteractionInstance.set('loc_text', data.data.twitter.user.location);
		}
	}

	InteractionInstance.save();

	io.sockets.on('connection', function (socket) {
		socket.emit('intro', InteractionInstance);
	});
	

});

